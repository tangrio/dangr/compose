# PHP-FPM local build

We need a PDO database driver in this image, so build it locally

```
docker build -t php:local .
```

Then use it later in the docker-compose file or directly by:
```
docker run -d php:local
```
